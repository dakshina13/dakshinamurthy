#include <stdio.h>
#include <math.h>
struct points
{
	float px,py,rad,ang,inpAng,cx,cy;
	float rAng,rx,ry;
};
int innp();
struct points inppoint();
float findRad(float ,float,float,float);
float findAng(float ,float );
float findRAng(float ,float );
void evalResPoint(struct points [],int );
void outRes(struct points );
int main()
{
	int n;
	n=innp();
	struct points a[n];
	printf("Enter the center,point,angle of rotation in the format x,y x1,y1 angle");
	for(int i=0;i<n;i++)
		a[i]=inppoint();
	evalResPoint(a,n);
	for(int i=0;i<n;i++)
		outRes(a[i]);
}
int innp(){
	int n;
	printf("Enter the number of points");
	scanf("%i",&n);
	return n;
}
struct points inppoint()
{
	struct points b;
	scanf("%f,%f %f,%f %f",&b.cx,&b.cy,&b.px,&b.py,&b.inpAng);
	b.rad=findRad(b.cx,b.cy,b.px,b.py);
	b.ang=findAng(b.px,b.py);
	b.rAng=findRAng(b.inpAng,b.ang);
	return b;
}
float findRad(float x,float y,float x1,float y1){
	return sqrt(pow(x-x1,2)+pow(y-y1,2));
}
float findAng(float x1,float y1){
	return atan(y1/x1);
}
float findRAng(float a1,float a2){
	return a1+a2;
}
void evalResPoint(struct points b[],int n){
	for(int i=0;i<n;i++){
		b[i].rx=b[i].rad*cos(b[i].rAng);
		b[i].ry=b[i].rad*sin(b[i].rAng);
	}
}
void outRes(struct points b){
	printf("Rotating (%f,%f) %f radians about (%f,%f) yields (%f,%f)",b.px,b.py,b.inpAng,b.cx,b.cy,b.rx,b.ry);
}