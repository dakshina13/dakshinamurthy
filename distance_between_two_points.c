#include <stdio.h>
#include <math.h>
struct point{
    int x,y;
};
struct point ind();
double evald(struct point s,struct point t );
void outd(struct point p,struct point q,double d);
int main()
{
    struct point p1,p2;
    double d;
    p1=ind();
    p2=ind();
    d=evald(p1,p2);
    outd(p1,p2,d);
    return 0;
}
struct point ind(){
    struct point p;
    printf("Enter the x and y coordinates of first point in the format x,y \n");
    scanf("%i,%i",&p.x,&p.y);
    return p;
}
double evald(struct point s,struct point t )
{
    double d1=(s.x-t.x)*(s.x-t.x)+(s.y-t.y)*(s.y-t.y);
    double d=sqrt(d1);
    return d;
}
void outd(struct point p,struct point q,double d){
    printf("THe distance between the two points (%i,%i) and (%i,%i) is %f",p.x,p.y,q.x,p.y,d);
}
