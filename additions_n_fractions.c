#include <stdio.h>
struct fraction{
int n,d;
};
int nf();
struct fraction in(int i);
struct fraction eval(struct fraction h[],int x);
int evalGcd(int a,int b);
void outf(struct fraction f[],struct fraction r,int n);

int main()
{
    int n;
	n=nf();
	struct fraction f[n],r;
    for(int i=0;i<n;i++)
        f[i]=in(i);
    r=eval(f,n);
    outf(f,r,n);
}
int nf(){
    int n;
    printf("number of fractions ");
    scanf("%i",&n);
    return n;
}
struct fraction in(int i)
{  
    struct fraction g;
    printf("Enter the fraction no.%i ",i+1);
    scanf("%i/%i",&g.n,&g.d);
    return g;
}

struct fraction eval(struct fraction h[],int x)
{
	struct fraction r2=h[0];
	int gcd;
	if(x>1){
        for(int i=1;i<x;i++){
            struct fraction r1=h[i];
            r2.n=r1.n*r2.d+r1.d*r2.n;
            r2.d=r2.d*r1.d;
        }
    }
	gcd=evalGcd(r2.n,r2.d);
	r2.n=r2.n/gcd;
	r2.d=r2.d/gcd;
    return r2;
}
int evalGcd(int a,int b){
    int gcd;
    for(int i=1;i<=a&&i<=b;i++)
        if(a%i==0&&b%i==0)
            gcd=i;
    return gcd;
}
void outf(struct fraction f[],struct fraction r,int n){
	printf("The sum of the fractions ");
	for(int i=0;i<n-1;i++)
		printf("%i/%i+ ",f[i].n,f[i].d);
    printf("%i/%i",f[n].n,f[n].d);
    printf(" is %i/%i \n",r.n,r.d);
}
/**
void outf(struct fraction f[],struct fraction r,int n){
	printf("The sum of the fractions ");
	for(int i=0;i<n;i++){
		printf("%i/%i",f[i].n,f[i].d);
		if(i<n-1)
			printf("+");
    }
    printf(" is %i/%i \n",r.n,r.d);
}
**/